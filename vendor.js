const csvdata = require('csvdata');
const dataFile = require('./dataFile');

const options = {
	empty: false,
	header: dataFile.headerString,
	delimiter: '|'
};

let data = [];
let i = 0;

const uniqueKeyPrefix = '20180917:19:33:09:';

let outFile = './output.csv';

if (process.argv.length > 2){
	// Output file name given.
	outFile = process.argv[2];
}

for(division of dataFile.divisions){
	for(vendor of dataFile.vendors){
		for(item of dataFile.items){
			for(new_date of dataFile.new_dates){

				i = i + 1;

				let newEntry = {};
				
				newEntry.UNIQUE_KEY = uniqueKeyPrefix + i;
				newEntry.SOURCE_SYSTEM = 'Demo_Script';
				newEntry.TRANSACTION_TYPE = 'INV';
				newEntry.STOCK_RECEIPT_NUMBER = i;
				newEntry.PO_NUMBER = i;
				newEntry.PO_LINE_NUMBER = i;
				newEntry.PO_DATE = new_date;
				newEntry.STOCK_RECEIPT_DATE = new_date;
				newEntry.ITEM_NUMBER = item;
				newEntry.QUANTITY = dataFile.date_quantity_dict[new_date];
				newEntry.UOM_CODE = 'CS';
				newEntry.BASE_QTY = 1;
				newEntry.BASE_UOM_CODE = 'CS';
				newEntry.UOM_CONVERSION_RATE = 1.0;
				newEntry.CURRENCY_CODE = 'USD';
				newEntry.PO_COST = dataFile.item_sp_dict[item]; ;
				newEntry.BASE_UOM_COST = '';
				newEntry.PO_LINE_AMOUNT ='';
				newEntry.VENDOR_NUMBER = vendor;
				newEntry.VENDOR_SITE_NUMBER='';
				newEntry.LANDED_COST = '';
				newEntry.FOB='';
				newEntry.ITEM_GL_ACCOUNT='';
				newEntry.DROP_SHIP_CUST_NUMBER='';
				newEntry.DROP_SHIP_CUST_SITE='';
				newEntry.WAREHOUSE_NUMBER = division;
				newEntry.WAREHOUSE_SEGMENT= division;
				newEntry.INVOICE_NUMBER = item + new_date;
				newEntry.INVOICE_LINE_NUMBER = 1;
				newEntry.INVOICE_DATE = new_date;
				newEntry.INVOICE_AMOUNT= newEntry.PO_COST * newEntry.QUANTITY;;
				newEntry.INVOICE_PAID_DATE= new_date;
				newEntry.PAID_IN_TERMS_FLAG='';
				newEntry.REPORT_START_DATE = new_date;
				newEntry.REPORT_END_DATE = new_date;
				newEntry.REPORT_DATE = new_date;
				newEntry.REPORT_DATE_JULIAN='';
				newEntry.STATUS='';
				newEntry.CREATED_BY='Demo_Script';
				newEntry.CREATION_DATE='';
				newEntry.INTERFACE_STATUS='NEW';
				newEntry.PACK_WEIGHT = 1;
				newEntry.DIVISIONAL_MASTER_VENDOR='';
				newEntry.REVISION_NUMBER='';
				
				data.push(newEntry);
			}
		}
	}
}

csvdata.write(outFile, data, options);

